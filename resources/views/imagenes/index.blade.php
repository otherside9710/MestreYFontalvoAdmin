@extends('layouts.app')
@section('breadcrumbs')
    @include('__partials.breadcrumbs', [
        'data' => [
            ['route' => 'home', 'name' => 'Home'],
            ['route' => '', 'name' => 'Imagenes']
        ],
        'show_add_button' => false,
        'add_button_route' => route('home')
    ])
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <center><h5>IMAGENES</h5></center>
                    <br><br>
                    <div class="row">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Url</th>
                                <th scope="col">Contenido</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($imagenes as $img)
                                <tr>
                                    <th scope="row">{{$img->id}}</th>
                                    <td>{{$img->nombre}}</td>
                                    <td>{{$img->url}}</td>
                                    <td>
                                        <form action="{{route('img.edit', $img->id)}}">
                                            <button class="btn btn-outline-success">Ver</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
