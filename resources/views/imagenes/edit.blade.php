@extends('layouts.app')
@section('breadcrumbs')
    @include('__partials.breadcrumbs', [
        'data' => [
            ['route' => 'home', 'name' => 'Home'],
            ['route' => 'img', 'name' => 'Imagenes'],
            ['route' => '', 'name' => 'Editar Imagen']
        ],
        'show_add_button' => false,
        'add_button_route' => route('home')
    ])
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <center><h5>EDITAR IMAGEN</h5></center>
                    <br><br>
                    <form action="{{route('img.update')}}" enctype="multipart/form-data" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$img->id}}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <img src="{{$img->url}}" height="380px" width="490px" alt="...">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row">
                                    <label>Nombre</label>
                                    <input type="text" required name="title" class="form-control"
                                           value="{{$img->nombre}}">
                                </div>
                                <br>
                                <div class="row">
                                    <label>Descripcion</label>
                                    <input type="text" required name="desc" class="form-control"
                                           value="{{$img->descripcion}}">
                                </div>
                                <br>
                                <div class="row">
                                    <button class="btn btn-outline-success">Actualizar</button>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <input type="file" class="form-control" name="image">
                            </div>
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
