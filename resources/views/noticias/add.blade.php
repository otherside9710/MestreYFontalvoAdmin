@extends('layouts.app')
@section('breadcrumbs')
    @include('__partials.breadcrumbs', [
        'data' => [
            ['route' => 'home', 'name' => 'Home'],
            ['route' => 'notices', 'name' => 'Noticias'],
            ['route' => '', 'name' => 'Agregar Noticia']
        ],
        'show_add_button' => false,
        'add_button_route' => route('home')
    ])
@endsection
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css">
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <center><h5>AGREGAR NOTICIA</h5></center>
                    <br><br>
                    <form action="{{route('notice.save')}}" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <label>Titulo</label>
                            <textarea class="form-control" required id="text" name="titulo" cols="30"
                                      rows="2"></textarea>
                        </div>
                        <br>
                        <div class="row">
                            <label>Descripción</label>
                            <textarea class="form-control" required id="text" name="desc" cols="30"
                                      rows="10"></textarea>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-8">
                                <label>Seleccione Fotos:</label>
                                <input class="form-control" id="myDrop" name="images[]" type="file" multiple="multiple"/>
                            </div>
                        </div>
                        <br>
                        <center>
                            <button class="btn btn-outline-success" id="btn_save">Guardar</button>
                        </center>
                    </form>
                    <br>
                </div>
            </div>
        </div>
    </div>
@endsection
