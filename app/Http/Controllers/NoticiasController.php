<?php

namespace App\Http\Controllers;

use App\Noticias;
use App\NoticiasIMG;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class NoticiasController extends Controller
{
    public function index()
    {
        $noticias = Noticias::all();
        return view('noticias.index', ['noticias' => $noticias]);
    }

    public function add()
    {
        return view('noticias.add');
    }

    public function save(Request $request)
    {
        if ($request->images) {
            $this->upload($request);
        } else {
            $noticia = new Noticias();
            $noticia->titulo = $request->titulo;
            $noticia->descripcion = $request->desc;
            $noticia->save();

            Session::put('success', 'Noticia Guardada Correctamente.');
        }

        return redirect()->route('notices');
    }

    public function edit($id)
    {
        $notice = Noticias::where('id', $id)->first();
        return view('noticias.edit', ['noticia' => $notice]);
    }

    public function update(Request $request)
    {
        $noticia = Noticias::where('id', $request->id)->first();
        $noticia->titulo = trim($request->titulo);
        $noticia->descripcion = trim($request->desc);
        $noticia->update();
        Session::put('success', 'Noticia Actualizada Correctamente.');
        return redirect()->route('notices');
    }

    public function delete($id)
    {
        Noticias::where('id', $id)->delete();
        Session::put('success', 'Noticia Eliminada Correctamente.');
        return redirect()->route('notices');
    }

    public function upload(Request $request)
    {
        $noticia = new Noticias();
        $noticia->titulo = $request->titulo;
        $noticia->descripcion = $request->desc;
        $noticia->save();

        foreach ($request->images as $photo) {
            $ext = $photo->getClientOriginalExtension();
            if ($ext == 'jpg' || $ext == 'jpge' || $ext == 'png' || $ext == 'csv') {
                $str = Carbon::now() . $photo->getClientOriginalName();
                $name = str_replace(' ', '', $str);
                try {
                    $ruta = $photo->move(public_path() . '/images/notices/', $name);

                    $base = 'http://52.14.94.46/MestreYFontalvoAdmin/public/images/notices/';

                    $notIMG = new NoticiasIMG();
                    $notIMG->real_name = $photo->getClientOriginalName();
                    $notIMG->name_actually = $name;
                    $notIMG->ruta_server = $ruta;
                    $notIMG->url = $base . $name;
                    $notIMG->noticia = $noticia->id;

                    $notIMG->save();

                    $not = Noticias::where('id', $noticia->id)->first();
                    $not->img = "S";
                    $not->update();

                } catch (\Exception $e) {
                    Noticias::where('id', $noticia->id)->delete();
                    Session::put('error', 'Error al subir las fotos de la noticia');
                }
            } else {
                Session::put('error', 'El Archivo subido no tiene un formato valido, debe ser .PNG O .JPG');
                return redirect()->route('notices');
            }
        }
        Session::put('success', 'Noticia Guardada Correctamente.');
        return redirect()->route('notices');
    }

}
