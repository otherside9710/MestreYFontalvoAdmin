<?php

namespace App\Http\Controllers;

use App\Imagenes;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;


class ImagenesController extends Controller
{
    public function index()
    {
        $imagenes = Imagenes::all();
        return view('imagenes.index', ["imagenes" => $imagenes]);
    }

    public function edit($id)
    {
        $img = Imagenes::where('id', $id)->first();
        return view('imagenes.edit', ['img' => $img]);
    }

    public function update(Request $request)
    {
        $flag = true;
        $image = Imagenes::where('id', $request->id)->first();
        $image->nombre = trim($request->title);
        $image->descripcion = trim($request->desc);
        if ($request->image) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $ext = $file->getClientOriginalExtension();

                if ($ext == 'jpg' || $ext == 'jpge' || $ext == 'png' || $ext == 'csv') {
                    $str = Carbon::now() . $file->getClientOriginalName();
                    $name = str_replace(' ', '', $str);
                    $ruta = $file->move(public_path() . '/images/banners/', $name);
                    $base = 'http://52.14.94.46/MestreYFontalvoAdmin/public/images/banners/';

                    $image->real_name = $file->getClientOriginalName();
                    $image->name_actually = $name;
                    $image->ruta_server = $ruta;
                    $image->url = $base . $name;
                    $image->update();
                    $flag = false;
                } else {
                    Session::put('error', 'El Archivo subido no tiene un formato valido, debe ser .PNG O .JPG');
                    return redirect()->route('img');
                }
            }
        }
        if ($flag) {
            $image->update();
        }
        Session::put('success', 'Imagen Actualizada Correctamente.');
        return redirect()->route('img');
    }
}
